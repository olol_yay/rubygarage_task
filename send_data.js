$(document).ready(function(){
			$(".headline table").hide();
			$(".update").hide();
			$(".alert").hide();
			$(".headline").hover(function(){
				$(this).children("table").fadeIn();
			});
			$(".headline").mouseleave(function(){
				$(this).children("table").fadeOut();
			});
			$(".headline span").click(function(){
				$(this).parent().next(".for_jq").slideToggle();
			});
			
			
		});


function update_info(Id,Action,Data){
	//alert(Id+Action+Data);
	$.post("update.php",{ id: Id, action: Action, data: Data})
	.done(
		function(data){
	   		
	   		$('#main').html(data); 

	   		$(".headline table").hide();
		    $(document).ready(function(){

			$(".headline table").hide();
			$(".headline").hover(function(){
				$(this).children("table").fadeIn();
			});
			$(".headline").mouseleave(function(){
				$(this).children("table").fadeOut();
			});
			$(".headline span").click(function(){
				$(this).parent().next(".for_jq").slideToggle();
			});

			});


	});

}

function red_alert(str){
	$(".alert").hide();
	$(".alert").html(str);
	$(".alert").stop();
	$(".alert").fadeIn().delay(5000).fadeOut();
}

function new_project_popup(){
	$(".update").hide();
	var popup = "<h4>Enter name for your new project:</h4><input type=\"text\" id=\"newpr_input\"><button onclick=\"create_project()\">OK</button><button id=\"hide\">Cancel</button>";
	$(".update").html(popup);
	$(".update").fadeIn();
	$("#hide").click(function(){
		$(".update").fadeOut();
	});
}
function create_project(){
	var data = $("#newpr_input").val();
	if(data>50){
		red_alert("Project name is too big");
	} else if (!data){
		red_alert("Project name empty");
	} else {
		$(".update").fadeOut();
		update_info("","new_project",data);
	}
}

function delete_project(id){
	update_info(id,"del_project","");
}

function update_project_popup(id){
	$(".update").hide();
	var value = $("#project_"+id).text();
	var popup = "<h4>Change your project name:</h4><input type=\"text\" id=\"upd_project\" value=\""+value+"\"><button onclick=\"update_project("+id+")\">OK</button><button id=\"hide\">Cancel</button>";
	$(".update").html(popup);
	$(".update").fadeIn();
	$("#hide").click(function(){
		$(".update").fadeOut();
	});
}
function update_project(id){
	var data = $("#upd_project").val();
	if(!data){
		red_alert("Empty project name");
	} else if(data.length > 200){
		red_alert("Project name is too big");
	} else {
		$(".update").fadeOut();
		update_info(id,"upd_project",data);
	}
}


function add_task(id){

	var data = $("#input_"+id).val();
	if(!data){
		red_alert("Empty task text");
	} else if(data.length > 200){
		red_alert("Task text is too bif");
	} else {
		update_info(id,"new_task",data);
	};
}
function up_task(id){
	update_info(id,"up_task","");
}
function down_task(id){
	update_info(id,"down_task","");
}
function update_task_popup(id){
	$(".update").hide();
	var value = $("#task_"+id).text();
	var deadline = $("#deadline_"+id).text();
	var popup = "<h4>Change your task:</h4><input type=\"text\" id=\"upd_task\" value=\""+value+"\"><button onclick=\"update_task("+id+")\">OK</button><button id=\"hide\">Cancel</button><br><input type=\"text\"  class=\"deadline_input\" id=\"upd_deadline\" value=\""+deadline+"\" placeholder=\"You can enter deadline for your task here...\"> ";
	$(".update").html(popup);
	$(".update").fadeIn();
	$("#hide").click(function(){
		$(".update").fadeOut();
	});
}
function update_task(id){
	var data = $("#upd_task").val();
	var dead = $("#upd_deadline").val();
	if(!data){
		red_alert("Empty task text");
	} else if(data.length > 200 || dead.length > 50){
		red_alert("Task text is too big");
	} else {
		$(".update").fadeOut();
		update_info(id,"upd_task",data+"#^#"+dead);
	}
}
function delete_task(id){
	update_info(id,"del_task","");
}
function set_task_done(id){
	update_info(id,"done_task","");
}

