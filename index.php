<?php 
session_start();

if(!isset($_SESSION['userid'])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Task manager</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
	</script>
	<script src="send_data.js">
	</script>
</head>
<body>
<div class="logout"><span>You are logged in as <?php echo $_SESSION['username'] ?>. </span><a href="logout.php">logout</a></div>
<div class="update">
</div>
<div class="alert">
</div>

<h1>SIMPLE TODO LISTS<br><small>FOR RUBY GARAGE ;)</small></h1>
	<div class="wrapper" id="main">

	<?php 
		require_once('project.php');
		show();
	?>

	</div>
	<button class="add_project" onclick="new_project_popup()"><span class="icon"></span>Add TODO list</button>
</body>